package main

import (
	"fmt"
	"reflect"
)

func main(){
	// Integers
	var luckyNumber int = 7
	var anotherLuckyNumber = 3
	anotherAnotherLuckyNumber := 9

	// Floats
	luckyFloat := 3.3
	var luckyLuckyFloat float64 = 3.4

	//bool
	var iamInetlligent = true
	youAreDumb := false

	// Strings
	name := "Abhi The master"

	fmt.Println(luckyNumber, anotherLuckyNumber, anotherAnotherLuckyNumber, luckyFloat, luckyLuckyFloat, iamInetlligent, youAreDumb, name)

	fmt.Println(reflect.TypeOf(luckyLuckyFloat))
}