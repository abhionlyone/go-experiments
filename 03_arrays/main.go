package main

import (
	"fmt"
)

func main(){

	fmt.Println("We will look into arrays in this section")
	
	// Arrays
	var arr1 [5] int
	arr1[0] = 10
	arr1[3] = 20

	// Another way of declaring an array
	arr2 := [5]int{1,2,3,4,5}

	// Slices: Similar to arrays but without size declaration
	slice1 := []int{1,2,34,5}

	// Use append to push a new element into a slice
	slice1 = append(slice1, 99)

	fmt.Println(arr1)
	fmt.Println(arr2)
	fmt.Println(slice1)
}