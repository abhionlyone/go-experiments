package main

import (
	"fmt"
)

func main() {
	var name = "abhilash"
	age := 29
	fmt.Println(name, age)

	if name == "abhi" && age == 28 {
		fmt.Println("Yo!!")
	} else if age == 27 {
		fmt.Println("Hehe")
	} else {
		fmt.Println("Cry")
	}
}
