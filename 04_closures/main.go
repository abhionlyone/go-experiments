package main

import "fmt"

func adder() func(int) int {
	sum := 0
	return func(num int) int {
		fmt.Println(num, "Sum is", sum, &sum)
		sum += num
		return sum
	}
}

func main() {
	summer := adder()
	for i := 0; i <= 10; i++ {
		fmt.Println(summer(i))
	}

	fmt.Println("Final", &summer)
}
