package main

import "fmt"

func main() {
	a := "abhilash"
	fmt.Println(a)
	changeMyname(&a)
	fmt.Println(a)
}

func changeMyname(name *string) string {
	*name = "Reddy"
	return *name
}
