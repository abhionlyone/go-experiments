package main

import (
	"fmt"
)

func main(){

	fmt.Println("We will look into maps in this section")
	
	myMap := make(map[string]string)
	myMap["name"] = "Abhilash"
	myMap["place"] = "Hindupur"

	// Another way to initialize a map

	myAnotherMap := map[string]string{"name":"Reddy","age":"26"}

	fmt.Println(myMap)
	fmt.Println(myAnotherMap)
}