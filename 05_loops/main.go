package main

import "fmt"

func main(){
	fmt.Println("Loops in Go...")

	var i = 1

	for i < 100 {
		fmt.Println(i)
		i++
	}

	arr1 := []int{1,2,3,4,5}

	for _, id := range arr1 {
		fmt.Println(id)
	}

	map1 := map[string]string{"abhilash":"26", "reddy": "28"}

	for k, v := range map1 {
		fmt.Println(k, v)
	}

	for k:= range map1 {
		fmt.Println(k)
	}

	for _, v := range map1 {
		fmt.Println(v)
	}

}