package main

import (
	"fmt"
)

func main() {
	var arr1 [2]string // Array

	arr1[0] = "Abhilash"

	var map1 = make(map[string]string)
	map1["name"] = "Abhilash"

	map2 := map[string]string{"name": "Reddy"}

	m := map[string]interface{}{}

	m["me"] = "ABHI"
	m["meta"] = map1
	m["another_meta"] = map2
	m["arr1"] = arr1

	fmt.Println(m)
}
